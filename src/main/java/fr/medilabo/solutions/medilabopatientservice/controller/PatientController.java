package fr.medilabo.solutions.medilabopatientservice.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.medilabo.solutions.medilabopatientservice.config.BadRequestException;
import fr.medilabo.solutions.medilabopatientservice.models.Patient;
import fr.medilabo.solutions.medilabopatientservice.services.PatientService;

@RestController
@RequestMapping("/patient")
public class PatientController {

    private final PatientService service;

    public PatientController(PatientService service) {
        this.service = service;
    }

    @GetMapping("/search")
    public ResponseEntity<List<Patient>> searchByName(@RequestParam String name) {
        return new ResponseEntity<>(this.service.searchByName(name), HttpStatus.OK);
    }

    @GetMapping("")
    public ResponseEntity<Patient> getPatient(@RequestParam int id) throws BadRequestException {
        return new ResponseEntity<>(this.service.getPatientbyId(id), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<String> createPatient(@RequestBody Patient patient) throws BadRequestException {
        this.service.createPatient(patient);
        return ResponseEntity.status(HttpStatus.CREATED).body("{\"message\": \"Patient successfully created\"}");
    }

    @PutMapping("")
    public ResponseEntity<String> updatePatient(@RequestBody Patient patient) throws BadRequestException {
        this.service.updatePatient(patient);
        return ResponseEntity.status(HttpStatus.CREATED).body("{\"message\": \"Patient successfully updated\"}");
    }

    @DeleteMapping("")
    public ResponseEntity<String> deletePatient(@RequestParam int id) throws BadRequestException {
        this.service.deletePatient(id);
        return ResponseEntity.status(HttpStatus.CREATED).body("{\"message\": \"Patient successfully removed\"}");
    }
}
