package fr.medilabo.solutions.medilabopatientservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.medilabo.solutions.medilabopatientservice.models.Patient;

public interface PatientRepository extends JpaRepository<Patient, Integer> {

}
