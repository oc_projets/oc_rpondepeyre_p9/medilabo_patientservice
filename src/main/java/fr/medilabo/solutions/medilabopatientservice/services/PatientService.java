package fr.medilabo.solutions.medilabopatientservice.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import fr.medilabo.solutions.medilabopatientservice.config.BadRequestException;
import fr.medilabo.solutions.medilabopatientservice.models.Patient;
import fr.medilabo.solutions.medilabopatientservice.repositories.PatientRepository;
import jakarta.transaction.Transactional;

@Service
public class PatientService {

    private final PatientRepository repository;

    public PatientService(PatientRepository repository) {
        this.repository = repository;
    }

    public List<Patient> searchByName(String name) {
        return this.repository.findAll().stream()
                .filter(patient -> patient.getNom().toLowerCase().contains(name.toLowerCase()) ||
                        patient.getPrenom().toLowerCase().contains(name.toLowerCase()) ||
                        (patient.getNom() + " " + patient.getPrenom()).toLowerCase().contains(name.toLowerCase()) ||
                        (patient.getPrenom() + " " + patient.getNom()).toLowerCase().contains(name.toLowerCase()))
                .collect(Collectors.toList());
    }

    public Patient getPatientbyId(int id) throws BadRequestException {
        Optional<Patient> patient = repository.findById(id);
        if (patient.isPresent()) {
            return patient.get();
        } else {
            throw new BadRequestException("No User found with specified identifier");
        }
    }

    @Transactional
    public void createPatient(Patient patient) throws BadRequestException {
        if (patient.getId() == null) {
            this.repository.save(patient);
        } else {
            throw new BadRequestException("You can't create a new patient with a predefined identifier");
        }
    }

    @Transactional
    public void updatePatient(Patient patient) throws BadRequestException {
        if (patient.getId() != null) {
            this.repository.save(patient);
        } else {
            throw new BadRequestException("You must specify patient identifier to perfom this update");
        }
    }

    @Transactional
    public void deletePatient(int id) throws BadRequestException {
        Patient patient = this.getPatientbyId(id);
        this.repository.delete(patient);
    }

}
