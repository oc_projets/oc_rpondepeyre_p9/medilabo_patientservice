package fr.medilabo.solutions.medilabopatientservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedilaboPatientServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedilaboPatientServiceApplication.class, args);
	}

}
