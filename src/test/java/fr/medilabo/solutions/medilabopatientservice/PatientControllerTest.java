package fr.medilabo.solutions.medilabopatientservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import fr.medilabo.solutions.medilabopatientservice.config.BadRequestException;
import fr.medilabo.solutions.medilabopatientservice.controller.PatientController;
import fr.medilabo.solutions.medilabopatientservice.models.Patient;
import fr.medilabo.solutions.medilabopatientservice.services.PatientService;

@ExtendWith(MockitoExtension.class)
public class PatientControllerTest {

    @Mock
    PatientService service;
    @InjectMocks
    PatientController controller;

    @Test
    public void searchByName() {
        this.controller.searchByName("name");
        verify(service).searchByName("name");
    }

    @Test
    public void getPatientbyId() throws BadRequestException {
        when(service.getPatientbyId(1)).thenReturn(new Patient());
        ResponseEntity<Patient> result = this.controller.getPatient(1);
        assertEquals(result.getStatusCode(), HttpStatus.OK);
        assertNotNull(result.getBody());
    }

    @Test
    public void createPatient() throws BadRequestException {
        doNothing().when(service).createPatient(any());
        ResponseEntity<String> result = this.controller.createPatient(new Patient());
        assertEquals(result.getStatusCode(), HttpStatus.CREATED);
    }

    @Test
    public void updatePatient() throws BadRequestException {
        doNothing().when(service).updatePatient(any());
        ResponseEntity<String> result = this.controller.updatePatient(new Patient());
        assertEquals(result.getStatusCode(), HttpStatus.CREATED);
    }

    @Test
    public void deletePatient() throws BadRequestException {
        doNothing().when(service).deletePatient(1);
        ResponseEntity<String> result = this.controller.deletePatient(1);
        assertEquals(result.getStatusCode(), HttpStatus.CREATED);
    }

}
