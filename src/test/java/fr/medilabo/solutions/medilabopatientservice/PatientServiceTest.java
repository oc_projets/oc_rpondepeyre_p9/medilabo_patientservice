package fr.medilabo.solutions.medilabopatientservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.medilabo.solutions.medilabopatientservice.config.BadRequestException;
import fr.medilabo.solutions.medilabopatientservice.models.Patient;
import fr.medilabo.solutions.medilabopatientservice.repositories.PatientRepository;
import fr.medilabo.solutions.medilabopatientservice.services.PatientService;

@ExtendWith(MockitoExtension.class)
public class PatientServiceTest {

    @Mock
    PatientRepository repository;

    @InjectMocks
    PatientService service;

    @Test
    public void search_casePrenom() {
        List<Patient> full = new ArrayList<>();
        Patient patient = new Patient();
        patient.setNom("Doe");
        patient.setPrenom("John");
        full.add(patient);

        when(repository.findAll()).thenReturn(full);

        List<Patient> result = service.searchByName("Joh");

        assertEquals(1, result.size());
    }

    @Test
    public void search_caseNom() {
        List<Patient> full = new ArrayList<>();
        Patient patient = new Patient();
        patient.setNom("Doe");
        patient.setPrenom("John");
        full.add(patient);

        when(repository.findAll()).thenReturn(full);

        List<Patient> result = service.searchByName("Do");

        assertEquals(1, result.size());
    }

    @Test
    public void search_caseNomPrenom() {
        List<Patient> full = new ArrayList<>();
        Patient patient = new Patient();
        patient.setNom("Doe");
        patient.setPrenom("John");
        full.add(patient);

        when(repository.findAll()).thenReturn(full);

        List<Patient> result = service.searchByName("Doe Jo");

        assertEquals(1, result.size());
    }

    @Test
    public void search_casePrenomNom() {
        List<Patient> full = new ArrayList<>();
        Patient patient = new Patient();
        patient.setNom("Doe");
        patient.setPrenom("John");
        full.add(patient);

        when(repository.findAll()).thenReturn(full);

        List<Patient> result = service.searchByName("John Do");

        assertEquals(1, result.size());
    }

    @Test
    public void search_caseEmpty() {
        List<Patient> full = new ArrayList<>();
        Patient patient = new Patient();
        patient.setNom("Doe");
        patient.setPrenom("John");
        full.add(patient);

        when(repository.findAll()).thenReturn(full);

        List<Patient> result = service.searchByName("ABCDE");

        assertEquals(0, result.size());
    }

    @Test
    public void getPatient_Ok() throws BadRequestException {
        when(repository.findById(1)).thenReturn(Optional.of(new Patient()));
        Patient result = this.service.getPatientbyId(1);
        verify(repository).findById(1);
        assertNotNull(result);
    }

    @Test
    public void getPatient_Ko() {
        when(repository.findById(1)).thenReturn(Optional.empty());
        assertThrows(BadRequestException.class, () -> {
            this.service.getPatientbyId(1);
        }, "No User found with specified identifier");
    }

    @Test
    public void createPatient_Ok() throws BadRequestException {
        Patient patient = new Patient();
        patient.setNom("nom");
        patient.setPrenom("prenom");
        patient.setDatedenaissance(LocalDate.now());
        patient.setAdresse("Addresse");
        patient.setGenre("M");
        patient.setTelephone("00000");
        patient.setId(null);
        this.service.createPatient(patient);
        verify(repository).save(patient);
    }

    @Test
    public void createPatient_Ko() {
        Patient patient = new Patient();
        patient.setNom("nom");
        patient.setPrenom("prenom");
        patient.setDatedenaissance(LocalDate.now());
        patient.setAdresse("Addresse");
        patient.setGenre("M");
        patient.setTelephone("00000");
        patient.setId(1);
        assertThrows(BadRequestException.class, () -> {
            this.service.createPatient(patient);
        }, "You can't create a new patient with a predefined identifier");

    }

    @Test
    public void updatePatient_Ok() throws BadRequestException {
        Patient patient = new Patient();
        patient.setNom("nom");
        patient.setPrenom("prenom");
        patient.setDatedenaissance(LocalDate.now());
        patient.setAdresse("Addresse");
        patient.setGenre("M");
        patient.setTelephone("00000");
        patient.setId(1);
        this.service.updatePatient(patient);
        verify(repository).save(patient);
    }

    @Test
    public void updatePatient_Ko() {
        Patient patient = new Patient();
        patient.setNom("nom");
        patient.setPrenom("prenom");
        patient.setDatedenaissance(LocalDate.now());
        patient.setAdresse("Addresse");
        patient.setGenre("M");
        patient.setTelephone("00000");
        patient.setId(null);
        assertThrows(BadRequestException.class, () -> {
            this.service.updatePatient(patient);
        }, "You must specify patient identifier to perfom this update");

    }

    @Test
    public void deletePatient_Ok() throws BadRequestException {
        when(repository.findById(1)).thenReturn(Optional.of(new Patient()));
        this.service.deletePatient(1);
        verify(repository).findById(1);
        verify(repository).delete(any());
    }

    @Test
    public void deletePatient_Ko() {
        when(repository.findById(1)).thenReturn(Optional.empty());
        assertThrows(BadRequestException.class, () -> {
            this.service.deletePatient(1);
        }, "No User found with specified identifier");
    }
}
